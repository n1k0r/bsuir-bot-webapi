package main

type Queue struct {
	ID     int64  `json:"id"`
	ChatID int64  `json:"chat_id"`
	Name   string `json:"name"`
	IsOpen bool   `json:"is_open"`
}

type Participant struct {
	ID        int64   `json:"id"`
	Position  int64   `json:"position"`
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Username  string  `json:"username"`
	Requests  []int64 `json:"requests"`
}
