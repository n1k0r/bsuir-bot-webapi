package main

import "github.com/julienschmidt/httprouter"

func getRouter(cacheDuration string) *httprouter.Router {
	router := httprouter.New()
	router.GET("/queue", Cached(listHandler, cacheDuration))
	router.GET("/queue/:id", Cached(detailHandler, cacheDuration))
	return router
}
