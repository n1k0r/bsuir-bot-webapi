package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func listHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	list := make([]Queue, 0, 8)

	rows, err := db.Query("SELECT id, chat_id, name, is_open FROM queue")
	if err != nil {
		log.Print(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	for rows.Next() {
		var queue Queue
		rows.Scan(&queue.ID, &queue.ChatID, &queue.Name, &queue.IsOpen)
		list = append(list, queue)
	}

	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	enc.Encode(list)
}

func detailHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "id have to be a number")
		return
	}

	var queue Queue
	err = db.QueryRow("SELECT id, chat_id, name, is_open FROM queue WHERE id = ?", id).Scan(
		&queue.ID,
		&queue.ChatID,
		&queue.Name,
		&queue.IsOpen,
	)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "id not found")
		return
	}

	requests := make(map[int64][]int64)
	rows, err := db.Query("SELECT sender_id, position FROM swaprequest WHERE queue_id = ?", id)
	if err != nil {
		log.Print(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	for rows.Next() {
		var request struct {
			sender int64
			pos    int64
		}
		rows.Scan(&request.sender, &request.pos)
		requests[request.sender] = append(requests[request.sender], request.pos)
	}

	participants := make([]Participant, 0, 16)
	rows, err = db.Query("SELECT id, position, first_name, last_name, username FROM participant WHERE queue_id = ?", id)
	if err != nil {
		log.Print(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	for rows.Next() {
		var participant Participant
		var pos sql.NullInt64
		rows.Scan(&participant.ID, &pos, &participant.FirstName, &participant.LastName, &participant.Username)
		if pos.Valid {
			participant.Position = pos.Int64
		} else {
			participant.Position = 0
		}

		participant.Requests = requests[participant.ID]
		if len(participant.Requests) == 0 {
			participant.Requests = make([]int64, 0)
		}
		participants = append(participants, participant)
	}

	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	enc.Encode(struct {
		Queue        Queue         `json:"queue"`
		Participants []Participant `json:"participants"`
	}{queue, participants})
}
