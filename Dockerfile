FROM golang:1.13-alpine AS builder

RUN apk add --no-cache g++ git

WORKDIR /app
COPY . .

RUN go get -d -v
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o webapi

RUN mkdir -p root/bin root/lib \
    && cp webapi root/bin/ \
    && cp /lib/ld-musl-x86_64.so.1 root/lib/


FROM scratch
COPY --from=builder /app/root/ /
ENTRYPOINT ["webapi"]
