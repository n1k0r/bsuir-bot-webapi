package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func showHelp() {
	fmt.Printf("Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	var addr, cacheLifetime, path string
	flag.StringVar(&addr, "addr", ":8000", "listen address")
	flag.StringVar(&cacheLifetime, "cache", "100ms", "cache lifetime")
	flag.StringVar(&path, "path", "", "path to database")
	flag.Parse()

	args := len(flag.Args())
	if args > 0 || path == "" {
		showHelp()
		os.Exit(1)
	}

	info, err := os.Stat(path)
	if os.IsNotExist(err) || info.IsDir() {
		fmt.Printf("'%s' not found\n", path)
		os.Exit(1)
	}

	source := fmt.Sprintf("%s?mode=ro", path)
	db, err = sql.Open("sqlite3", source)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	router := getRouter(cacheLifetime)
	log.Fatal(http.ListenAndServe(addr, router))
}
