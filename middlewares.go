package main

import (
	"log"
	"net/http"
	"net/http/httptest"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"
)

type cacheRecord struct {
	response *httptest.ResponseRecorder
	expire   time.Time
}

var cache = make(map[string]*cacheRecord)
var mux = sync.RWMutex{}

func Cached(h httprouter.Handle, duration string) httprouter.Handle {
	parsedDuration, err := time.ParseDuration(duration)
	if err != nil {
		log.Fatal(err)
	}

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		path := r.URL.Path

		mux.RLock()
		cachedPage := cache[path]
		mux.RUnlock()

		if cachedPage == nil || time.Now().After(cachedPage.expire) {
			mux.Lock()
			if cache[path] == nil || time.Now().After(cache[path].expire) {
				rec := httptest.NewRecorder()
				h(rec, r, ps)

				cache[path] = &cacheRecord{
					rec,
					time.Now().Add(parsedDuration),
				}
			}
			mux.Unlock()
		}

		mux.RLock()
		cachedPage = cache[path]
		mux.RUnlock()

		resp := cachedPage.response

		wHeader := w.Header()
		for k, v := range resp.HeaderMap {
			if k == "Date" {
				continue
			}
			wHeader[k] = v
		}
		w.WriteHeader(resp.Code)

		w.Write(resp.Body.Bytes())
	}
}
